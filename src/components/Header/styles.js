import styled from 'styled-components'

export const Wrapper = styled.div`
    background-color: rgba(66, 75, 82, .4);
    padding: 10px;
`
Wrapper.displayName = 'HeaderWrapper';